/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koneksimysql;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
//atau bisa kita menggunakan
//import java.sql.*;

//import resutl jika tadi tidak pakai java.sql.*
import java.sql.ResultSet;

/**
 *
 * @author abah
 */

public class KoneksiMysql {

    //import variable yang nanti kita gunakan
    
    static final String DRIVER_ = "com.mysql.jdbc.Driver";
    static final String DB_ ="jdbc:mysql://localhost/fornetbeans";
    static final String USER_ = "root";
    static final String PASS_ = "1";
    
    //siapkan juga variable yang digunakan untuk menampung data
    static Connection koneksi;
    static Statement stmt;
    
    //buat variable untuk menampung result
    static ResultSet rs;
    
    public static void main(String[] args) {
        
        //untuk melakukan koneksi wajib menggunakan try catch
        try{
            //registrasi driver yang akan di gunakan
            Class.forName(DRIVER_);
            
            //buat koneksi ke database
            koneksi = DriverManager.getConnection(DB_, USER_, PASS_);
            
            //Tampung data dari koneksi
            stmt = koneksi.createStatement();
            
            //Print jika koneksi Berhasil
            System.out.println("Koneksi ke database Berhasil !!!");
            
            //buat query sql untuk mendapatkan isi table
            String query = "SELECT * FROM data";
            
            //eksekusi query
            rs =stmt.executeQuery(query);
            
            //tampilkan data dengan loop
            while(rs.next()){
                System.out.println("Nama : "+ rs.getString("nama"));
                System.out.println("Alamat : "+ rs.getString("alamat"));
            }
            
            //jika sudah berhasil close statement
            stmt.close();
            
            //dan juga koneksi
            koneksi.close();
            
        }catch(Exception error){
            //Print jika Gagal
            System.out.println("Koneksi ke database Gagal !!!");
        
        };
        
        
    }
    
}
