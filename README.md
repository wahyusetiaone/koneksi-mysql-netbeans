# Project Title

Simple Koneksi MySQL with Java (NetBeans)

## Getting Started

Git Pull or Download This Project

### Prerequisites

NetBeans and MySQL

### Installing

User and Database MySQL

```
    static final String DRIVER_ = "com.mysql.jdbc.Driver";
    static final String DB_ ="jdbc:mysql://localhost/fornetbeans";
    static final String USER_ = "root";
    static final String PASS_ = "1";

```

## Running the tests

Press Run or F6

## Youtube Tutorial

https://youtu.be/F3c_UmRPJmw


